const express = require("express");
const path = require("path");
const morgan = require("morgan");
const mongoose = require("mongoose");
const PORT = process.env.PORT || 8080;
const app = express();

const { notesRouter } = require("./controllers/notesController");
const { authRouter } = require("./controllers/authController");
const { userRouter } = require("./controllers/userController");
const { authMiddleware } = require("./middlewares/authMiddleware");
const { NodeCourseError } = require("./utils/errors");

app.use(express.json());
app.use(morgan("tiny"));

app.use("/api/auth", authRouter);
app.use("/api/users/me", [authMiddleware], userRouter);
app.use("/api/notes", [authMiddleware], notesRouter);

app.use((req, res, next) => {
  res.status(404).json({ message: "Not found" });
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({ message: err.message });
  }
  res.status(500).json({ message: err.message });
});

const start = async () => {
  try {
    await mongoose.connect(
      "mongodb+srv://Polina:Polina1510@cluster0.kn7xm.mongodb.net/notes?retryWrites=true&w=majority",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    );

    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
