const express = require("express");
const router = new express.Router();

const {
  getUserProfileById,
  deleteUserProfileById,
  changeUsersPasswordById,
} = require("../services/userService");

const { asyncWrapper } = require("../utils/apiUtils");
const { InvalidRequestError } = require("../utils/errors");

//getUserProfileById
router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const user = await getUserProfileById(userId);

    if (!user) {
      throw new InvalidRequestError("No user with such id found");
    }

    res.json({ user });
  })
);

//deleteUserProfileById
router.delete(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const user = await deleteUserProfileById(userId);

    if (!user) {
      throw new InvalidRequestError("No user with such id found");
    }

    res.json({ message: "User was deleted successful" });
  })
);

//changeUsersPasswordById
router.patch(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { oldPassword, newPassword } = req.body;

    await changeUsersPasswordById(userId, oldPassword, newPassword);

    res.status(200).json({ message: "Success" });
  })
);

module.exports = {
  userRouter: router,
};
