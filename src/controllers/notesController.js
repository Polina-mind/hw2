const express = require("express");
const router = express.Router();

const {
  getNotesByUserId,
  addNoteToUser,
  getNoteByIdForUser,
  updateNoteByIdForUser,
  deleteNoteByIdForUser,
  completeNote,
} = require("../services/notesService");

const { asyncWrapper } = require("../utils/apiUtils");
const { InvalidRequestError } = require("../utils/errors");

//getNotesByUserId
router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { offset, limit } = req.query;

    const notes = await getNotesByUserId(userId, offset, limit);

    res.json({ notes });
  })
);

//addNoteToUser
router.post(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await addNoteToUser(userId, req.body);
    res.json({ message: "Note created successfully" });
  })
);

//getNotesByIdForUser
router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const note = await getNoteByIdForUser(id, userId);

    if (!note) {
      throw new InvalidRequestError("No note with such id found!");
    }

    res.json({ note });
  })
);

//updateNoteByIdForUser
router.put(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;

    const note = await updateNoteByIdForUser(id, userId, data);

    console.log(note);

    if (!note) {
      throw new InvalidRequestError("No note with such id found!");
    }

    res.json({ message: "File was updated successful" });
  })
);

//deleteNoteByIdForUser
router.delete(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const note = await getNoteByIdForUser(id, userId);

    if (!note) {
      throw new InvalidRequestError("No note with such id found!");
    }

    await deleteNoteByIdForUser(id, userId);
    res.json({ message: "File was deleted successful" });
  })
);

//updateNoteByIdForUser
router.patch(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const note = await completeNote(id, userId);

    if (!note) {
      throw new InvalidRequestError("No note with such id found!");
    }

    res.json({ message: "File was updated successful" });
  })
);

module.exports = {
  notesRouter: router,
};
