const mongoose = require("mongoose");

const Note = mongoose.model("Note", {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  text: String,

  completed: {
    type: Boolean,
    default: false,
    required: true,
  },

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  __v: { type: Number, select: false },
});

module.exports = { Note };
