const express = require("express");
const router = express.Router();

const { registration, login } = require("../services/authService");
// const { InvalidRequestError } = require("../utils/errors");

const { asyncWrapper } = require("../utils/apiUtils");

router.post(
  "/register",
  asyncWrapper(async (req, res) => {
    const { username, password } = req.body;

    await registration({ username, password });

    res.json({ message: "Account created successfully!" });
  })
);

router.post(
  "/login",
  asyncWrapper(async (req, res) => {
    const { username, password } = req.body;

    const token = await login({ username, password });
    console.log(token);
    
    res.json({ message: "Success", jwt_token: token });
    
  })
);

//forgotPassword
router.post(
  "/forgot_password",
  asyncWrapper(async (req, res) => {
    res.json({ message: "New password sent to your email address!" });
  })
);

module.exports = {
  authRouter: router,
};
