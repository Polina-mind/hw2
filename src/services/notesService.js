const { Note } = require("../models/noteModel");

const getNotesByUserId = async (userId, offset, limit) => {
  offset = parseInt(offset) || 0;
  limit = parseInt(limit) || 5;

  const notes = await Note.find({ userId }).skip(offset).limit(limit);

  return notes;
};

const getNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({ _id: noteId, userId });
  return note;
};

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({ ...notePayload, userId });
  await note.save();
};

const updateNoteByIdForUser = async (noteId, userId, data) => {
  const note = await Note.findOneAndUpdate(
    { _id: noteId, userId },
    { $set: data }
  );
  return note;
};

const deleteNoteByIdForUser = async (noteId, userId) => {
  await Note.findOneAndRemove({ _id: noteId, userId });
};

const completeNote = async (id, userId) => {
  const note = await Note.findOne({ _id: id, userId });

  // const noteCompleted = note.completed ? true : false;

  if (note.completed) {
    await findOneAndUpdate({ _id: id, userId }, { $set: { completed: false } });
  } else {
    await findOneAndUpdate({ _id: id, userId }, { $set: { completed: true } });
  }
};

module.exports = {
  getNotesByUserId,
  addNoteToUser,
  getNoteByIdForUser,
  updateNoteByIdForUser,
  deleteNoteByIdForUser,
  completeNote,
};
